module.exports = class Game{
    constructor(){
        console.log("I am thinking of a Colour name... can you guess its letters? ");
        this.sPrompt = "Nice try., You can do it!";
        this.bDone = false;
        this.lives = 5;
        this.words = ["Blue", "Black", "Yellow", "Orange", "Green", "Purple"];
        this.correctGuesses =[];
        this.incorrectGuesses = [];
        this.lettersRevealed = 0;
        this.aResponses = []; 

        // Pick a random word
        this.word = this.words[Math.floor(Math.random() * this.words.length)];
        this.wordToGuess = this.word.toUpperCase();

        // Set up the answer answerArray variable 
        this.displayToPlayer = [];
        for (var i = 0; i < this.word.length; i++) {
            this.displayToPlayer[i] = '$';
        } 

        
    }

    takeATurn(sInput){
        
        this.aResponses = [];        
        if(!sInput.match(/[a-z]/i)){
            this.aResponses.push("Please enter an Alphabet only");
        }
        else if (sInput.length !== 1) {
            this.aResponses.push("Please enter a single letter only.");
        }
        if (this.correctGuesses.includes(sInput.toUpperCase())) 
        {
            this.aResponses.push(`You've already tried ${sInput.toUpperCase()}, and it was correct!`);
        }
        else if (this.incorrectGuesses.includes(sInput.toUpperCase())) 
        {
            this.aResponses.push(`You've already tried ${sInput.toUpperCase()}, and it was wrong!`);
        } 
        if (this.wordToGuess.includes(sInput.toUpperCase())) 
        {
            this.correctGuesses.push(sInput.toUpperCase());
            for (let i = 0; i < this.wordToGuess.length; i++) 
            {
                if (this.wordToGuess[i] == sInput.toUpperCase())
                {
                    this.displayToPlayer[i] = this.wordToGuess[i];
                    this.lettersRevealed++;
                }
            }
            if (this.lettersRevealed == this.wordToGuess.length){
                this.bDone = true;
                this.aResponses.push("You won!");
                this.aResponses.push("Let's Try Again"); //comment this line when run the program in console
                this.sPrompt="";
            }                
        }           
        else 
        {
            this.incorrectGuesses.push(sInput.toUpperCase());
            this.aResponses.push(`Nope, there's no ${sInput.toUpperCase()} in it!`);
            this.lives--;

        }
        if(this.bDone == false){
            var x =this.displayToPlayer.join("");
            this.aResponses.push(x); 
        }
        if(this.lives == 0){
            this.bDone = true;
            this.aResponses.push(`You lost! It was ${this.wordToGuess}`);
            this.aResponses.push("Let's Try again with a new word!"); //comment this line when run the program in console
            this.sPrompt="";
        }
        return this.aResponses;      
    }
        

    prompt(){
        return this.sPrompt;
    }
    isDone(){
        return this.bDone;
    }
}
