const input = require('readline-sync');
const Game = require('./Game.js');

const oGame = new Game();

while(!oGame.isDone()){
    const sInput = input.question(oGame.prompt());
    console.log(oGame.takeATurn(sInput));
}