const express = require('express');
const bodyParser = require("body-parser");
const Game = require("./Game.js");
const { response } = require('express');

// Create a new express application instance
const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("www"));

app.get("/users/:uname", (req, res) => {
    res.end("Hello " + req.params.uname); 
});


//const oGame = new Game();
var x =false;
    let oGames = {};
    app.post("/sms", (req, res) =>{
        
        res.setHeader('content-type', 'text/xml');
        let sFrom = req.body.From || req.body.from;
    
        if(!(sFrom in oGames)){
            oGames[sFrom] = new Game();
        }    
    
        let sMessage = req.body.Body|| req.body.body;

        //oGame.bDone1=false;
        if(x==false){
            res.end("<Response><Message>I am thinking of a Colour name ... can you guess its letters? </Message></Response>");
            x=true;
            
        }
        else{
        let aResponse = oGames[sFrom].takeATurn(sMessage);
        let sResponse = "<Response>";
        for(let n = 0; n < aResponse.length; n++){
            sResponse += "<Message>" + aResponse[n] + "</Message>";
        }
        sResponse += "<Message>" + oGames[sFrom].prompt() + "</Message>";
        res.end(sResponse + "</Response>");
        if(oGames[sFrom].isDone()){
            delete oGames[sFrom];
        }
    }
    });







var port = process.env.PORT || parseInt(process.argv.pop()) || 3000;

app.listen(port, () => console.log('Example app listening on port ' + port + '!'));