# Introduction
Hangman is a paper and pencil guessing game for two or more players. One player thinks of a word, phrase or sentence and the other(s) tries to guess it by suggesting letters within a certain number of guesses.
The same idea of this game is implemented in Node js and its user interface is setup as an interactive chatbot where the computer will ask you to guess the alphabets of a given word.

* Maximum 5 wrong guesses are allowed.
* Only Colours' name will be asked by the computer.
* The correct alphabet will be placed in its appropriate position in the word.
* The game also let the user know about the previously used alphabet, if the user input the same alphabet again.
---


# Requirements 
* Download the source code of the project.
* Open project folder in Visual Studio Code
* Open the terminal to run the project
* Check the installed version of nodejs by typing "node --version". if node is not pre-Installed then type "npm install" in the terminal to install nodejs
* To run the project in console then type "node IndexConsole.js" or 
* To the run the project in the local web window type "node IndexExpress.js" and browse "http://127.0.0.1:3000/" in your web browser.
* To run this project online, open the URL in your web browser "https://hangman-chat-ui-game.herokuapp.com/" 

# Copyright © Amandeep Singh - 2020
